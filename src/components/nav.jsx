import React, { Component } from 'react';

const NavBbar = (props) => {
    return (  
        <nav className="navbar navbar-light bg-light">
            <a className="navbar-brand" href="#">
                Nav bar{" "}
                <span className="badge badge-pill badge-secondary">
                    {props.totalCounters}
                </span>
            </a>
        </nav>
    );
}
 
export default NavBbar;
