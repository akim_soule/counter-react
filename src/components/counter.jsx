import React, { Component } from 'react'

class Counter extends Component {

    state = {
        count : this.props.counter.value,
    };

    renderTags(){
        return this.state.tags.length === 0 ? 
        <p>Il n'y a aucun tag</p> : 
        <ul>{this.state.tags.map(tag => <li key={tag}>{tag}</li>)}</ul>;
    }

    render() { 
        return (
        <div>
            <h4>Counter #{this.props.counter.id}</h4>
                <span 
                    className={this.getBadgeClasses()}
                    >
                        {this.formatCount()}
                </span>
            <button 
            onClick={() => this.props.onIncrement(this.props.counter)}
            className='btn btn-secondary btn-sm'>
                Increment
            </button>
            <button 
            onClick={()=> this.props.onDelete(this.props.counter.id)} 
            className='btn btn-danger btn-sm m-2' >
                Delete
            </button>
        </div>
        );
    }

    getBadgeClasses(){
        let classes = "badge m-2 badge-";
        classes += this.props.counter.value === 0 ? "warning" : "primary";
        return classes;
    }

    formatCount(){
        console.log(this.props.counter.value);
        const count = this.props.counter.value;
        return count === 0 ? 'Zero' : count;
    }
}
 
export default Counter;