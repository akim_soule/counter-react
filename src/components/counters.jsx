import React, { Component } from 'react';
import Counter from './counter';

class Counters extends Component {
    

    render() { 

        const {onReset, onDelete, onIncrement, counters} = this.props;

        return ( 
        <div>
            <button 
                onClick={onReset}
                className="btn btn-primary btn-sm m-2"

            >
                Reset
            </button>
            {counters.map((element) => 
            <Counter 
                    key={element.id} 
                    value={element.value}
                    onDelete={onDelete}
                    onIncrement={onIncrement}
                    counter={element}
                    />
                    )}
        </div> );
    }
}
 
export default Counters;