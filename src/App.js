import Nav from './components/nav';
import Counter from './components/counters';
import React, { Component } from 'react'
import './App.css';

class App extends Component {

  state = {  
    counters : [
        {id : 1, value : 0},
        {id : 2, value : 0},
        {id : 3, value : 0},
        {id : 4, value : 0},
        {id : 5, value : 0},
    ]
}

handleReset = () =>{
    const counters = this.state.counters.map(c => {
        c.value = 0;
        return c;
    });

    this.setState({counters : counters});
};

handleIncrement = (counter)=>{
    const counters = [...this.state.counters];
    const index = counters.indexOf(counter);
    counters[index] = {...counter};
    counters[index].value++;
    this.setState({counters});
}

handleDelete = (counterId) => {
    const counters = this.state.counters.filter( c => 
        c.id !== counterId);
        this.setState({counters : counters});
    console.log('Event Handler Called', counterId);
}
  render() { 
    return (
      <React.Fragment>
        <Nav 
            totalCounters={this.state.counters.filter(c => c.value > 0).length}
          />
        <main className='container'>
          <Counter
            counters={this.state.counters}
            onReset={this.handleReset}
            onIncrement={this.handleIncrement} 
            onDelete={this.handleDelete}
            />
        </main>
      </React.Fragment>
      
    );
  }
}
 
export default App;

